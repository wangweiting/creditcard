$(document)
  .ready(function() {
    $('.filter.menu .item')
      .tab()
    ;
    $('.ui.rating')
      .rating({
        clearable: true
      })
    ;
    $('.ui.sidebar')
      .sidebar({
        overlay: true
      })
      .sidebar('attach events', '.launch.button')
      
    ;
    $('.ui.dropdown')
      .dropdown()
    ;
    $('.modal')
      .modal('setting', 'transition', 'vertical flip')
    ;
    $('.login.modal')
      .modal('attach events', '.login.button','show')
    ;
    $('.apply.modal')
      .modal('attach events', '.apply.button','show')
    ;
    $("#register").click(function() {
      $(".resume").removeClass('disabled');
      $(".form.register").addClass('finished');
    });
  })
;